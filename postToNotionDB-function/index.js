const { Client } = require("/opt/node_modules/@notionhq/client"); // eslint-disable-line

const notion = new Client({
  auth: process.env.NOTION_API_TOKEN,
});

async function createNewPage(word, definition, synonyms) {
  const notionPost = await notion.pages.create({
    parent: {
      database_id: process.env.WORD_DATABASE_ID,
    },
    properties: {
      Word: {
        title: [
          {
            text: {
              content: word,
            },
          },
        ],
      },
      Definition: {
        rich_text: [
          {
            text: {
              content: definition,
            },
          },
        ],
      },
      Synonym: {
        rich_text: [
          {
            text: {
              content: synonyms,
            },
          },
        ],
      },
    },
  });
  return notionPost;
}

exports.handler = async (event) => {
  const data = JSON.parse(JSON.parse(JSON.parse(event.Records[0].body).Message));
  const notionResponse = await createNewPage(
    data.postWord,
    data.postDefinition,
    data.postSynonyms,
  );
  const response = {
    statusCode: 200,
    body: JSON.stringify(notionResponse),
  };
  return response;
};
