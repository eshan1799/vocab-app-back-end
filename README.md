# Vocab App Back-End

## Description ##
A serverless webapp that interacts with a Notion database, and leverages that data to generate wallpaper images.
* Populates images with data received from an open-source dictionary API
* Built using Lambda, API Gateway, SNS, S3
* Uses CI/CD pipelines to test, plan and deploy
* Infrastructure is built using Terraform

## Architecture ##
<br />
![image info](img/vocab-app-design.png)
