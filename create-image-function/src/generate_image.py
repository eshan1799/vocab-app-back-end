from PIL import Image, ImageDraw, ImageFont
import textwrap, random
import os, boto3, botocore, logging
import json

colours_list = [
    (219, 145, 145),
    (128, 78, 78),
    (205, 101, 101),
    (111, 107, 39),
    (234, 179, 197),
    (132, 143, 236),
    (36, 68, 54),
    (73, 39, 74),
    (104, 141, 10),
    (165, 73, 75),
    (77, 143, 152),
    (60, 83, 50),
    (112, 64, 116),
    (192, 140, 21),
    (54, 129, 253),
    (75, 83, 187),
    (138, 201, 10),
    (0, 153, 153),
    (76, 153, 0),
    (153, 76, 0),
    (102, 0, 0),
    (96, 96, 96),
    (128, 128, 128),
    (160, 160, 160),
    (64, 64, 64),
    (102, 0, 51),
    (51, 0, 102),
    (0, 0, 51),
    (0, 51, 51),
    (0, 102, 102),
    (0, 102, 51),
    (0, 76, 153),
    (0, 128, 255),
    (102, 102, 255),
    (59, 102, 135),
    (51, 166, 120),
    (182, 189, 135)
]

def get_font(font_size, font="segoeui.ttf"):
    my_font = ImageFont.truetype(font, font_size)
    return my_font

def rndm_colour():
    num = random.randint(0, len(colours_list)-1)
    return colours_list[num]

def create_block(text, width=45):
    wrapped_text = textwrap.wrap(text, width=width)
    block_text = '\n'.join(wrapped_text)
    return block_text

def upload_image(file_name, bucket, object_name=None):
    if object_name is None:
        object_name = os.path.basename(file_name)

    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except botocore.exceptions.ClientError as e:
        logging.error(e)
        return False
    return True

def generate_image(dictionary_data):
    image_size = (1440, 3120)
    colour = rndm_colour()
    my_image = Image.new("RGB", image_size, colour)
    draw = ImageDraw.Draw(my_image)

    spacing = 40
    count = 0
    position_height = 0
    font_sizes = (120, 60, 40)

    for key in dictionary_data:
        block_text = create_block(dictionary_data[key])
        font = get_font(font_sizes[count])
        _, _, block_width, block_height = draw.textbbox(( 0, 0 ), block_text, font=font)
        if count == 0:
            position_height = (image_size[1] - block_height) /3
        draw.text(((image_size[0] - block_width)/2, position_height), block_text, align="center", font=font, fill="white")
        position_height += block_height + spacing
        count += 1
    
    file_path = "/tmp/" + dictionary_data["postWord"] + ".jpg"  #### lambda
    # file_path = dictionary_data["word"] + ".jpg"  #### local
    my_image.save(file_path, quality=100)
    result = upload_image(file_path, "vocab-app-wallpapers")
    return result

def lambda_handler(event, context):
    message = json.loads(json.loads(json.loads(event["Records"][0]["body"])["Message"]))
    result = generate_image(message)
    return {
        'statusCode': 200,
        'body': result
    }
