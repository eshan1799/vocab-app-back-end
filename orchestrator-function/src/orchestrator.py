import json
import boto3, botocore

def lambda_handler(event, context):
    client = boto3.client('sns')
    body = json.dumps(event['body'])
    response = client.publish (
        TargetArn = 'arn:aws:sns:eu-west-2:753108572620:dict-data-topic',
        Message = json.dumps({'default': body}),
        MessageStructure = 'json'
    )
    return {
        'statusCode': 200,
        'body': json.dumps(response)
    }
